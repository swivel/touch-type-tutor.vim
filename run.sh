#!/usr/bin/env bash

git submodule init
git submodule update

vim '+source touchtutor.vim' './exercises'

