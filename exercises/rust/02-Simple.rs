use str::test

mod test {
    fn main() {
        let n = "this is a test";

        let isWorking = true;

        if isWorking {
            println!(n);
        }
    }
}
