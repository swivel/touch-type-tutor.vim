const obj = {
	foo: 'bar',
	baz: 45.02,
	quux: [12, 13]
};

const output = o => console.log(o);

output(obj);
