const util = require('util');

const obj = {
	foo: 'bar',
	baz: 45.02,
	quux: [12, 13]
};

const output = (obj) => {
	const formatted = util.inspect(obj);

	console.log(formatted);
}

output(obj);
