let g:ttt_startt=0
let g:ttt_time=0
let g:ttt_nchars=0
let g:ttt_cpm=0
let g:ttt_wpm=0

function! ttttimer#StartCounting()
  let g:ttt_startt = reltime()
  silent normal! qt
endfunction

function! ttttimer#StopCounting()
  silent normal! q
  let g:ttt_time = reltimefloat(reltime(g:ttt_startt))

  " let g:ttt_nchars = str2float(system('wc -m', bufnr('%')))
  let chars = substitute(strtrans(@t), '\v\^?\[?\<[A-Za-z0-9]{2}\>\<?[a-z][a-z]\>?', '', 'g')
  let chars = substitute(chars, '\v\^[A-Za-z]', ' ', 'g')
  let g:ttt_nchars = strlen(chars)

  let minutes = g:ttt_time / 60.0

  let g:ttt_cpm = float2nr(round(g:ttt_nchars / minutes))
  let g:ttt_wpm = float2nr(round(g:ttt_cpm / 5))
endfunction

