function! tttpanels#CreateOutput(where, synt)
  NoMatchParen

  if strlen(a:where) >= 2
    if a:where ==# "right"
      set splitright
      vert new
      set nosplitright
    else
      silent exe a:where . " new"
    endif
  else
    bel new
  endif

  setlocal nowrap
  setlocal nonumber
  setlocal norelativenumber
  setlocal noswapfile
  setlocal buftype=nofile
  setlocal bufhidden=delete

  if strlen(a:synt)
    exe 'setlocal syntax=' . a:synt
  else
    setlocal syntax=diff
  endif

  let dict = {
    \ 'view': winsaveview(),
    \ 'buffer': bufnr('%'),
    \ 'window': bufwinnr('%'),
    \ }

  DoMatchParen

  return dict
endfunction

function! tttpanels#IsOpen(name)
  if has_key(g:ttt_outs, a:name)
    let bufinfo = getbufinfo(g:ttt_outs[a:name].buffer)
    if len(bufinfo) >= 1
      return 1
    endif
  endif
  return 0
endfunction

function! tttpanels#CloseOut(name)
  if tttpanels#IsOpen(a:name)
    let out = get(g:ttt_outs, a:name)
    let win = get(out, 'window')

    exe win . 'wincmd w' | wincmd c

    call remove(g:ttt_outs, a:name)
  endif
endfunction
