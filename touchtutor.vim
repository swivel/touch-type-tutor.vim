source .vim/Colorizer/autoload/Colorizer.vim
source .vim/Colorizer/plugin/ColorizerPlugin.vim

source functions/tttpanels.vim
source functions/ttttimer.vim

let g:colorizer_bufleave = 0

let g:ttt_run = {}
let g:ttt_cur = {}
let g:ttt_outs = {}

let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'
let g:netrw_hide = 1

function! touchtutor#Finished()
  " Save file we're in, and where we're at in the file
  let g:ttt_cur = {
    \ 'view': winsaveview(),
    \ 'buffer': bufnr('%'),
    \ 'window': bufwinnr('%'),
    \ 'tail': expand('%:t'),
    \ 'path': expand('%:p'),
    \ }

  let g:ttt_success = 0

  " Run tests
  let output = system('./test.sh ' . shellescape(expand('%')))

  " Create output buffer if it doesn't exist
  if tttpanels#IsOpen('result') == 0
    let g:ttt_outs.result = tttpanels#CreateOutput('below', 'diff')
  endif

  " Focus on the output buffer
  exe g:ttt_outs.result.window . 'wincmd w'
  exe g:ttt_outs.result.buffer . 'buffer'

  " Set the buffer's name
  let g:ttt_outs.result['file'] = 'TouchTypeTutor: ' . g:ttt_cur.tail
  silent exe 'file '. escape(g:ttt_outs.result['file'], ' |\"')

  " Clear contents of output buffer
  silent normal! gg0"_dG

  " Check to see if the tests passed
  if strlen(output) ==# 1
    " If they did, open the explorer to move on to the next test
    call setline(1, ['Great job! NO MISTAKES! Now try another file!', ''])
    let g:ttt_success = 1
  else
    " Otherwise, restart the test
    call setline(1, ['Good job, you finished! Here is where you stumbled a bit...', ''])
    call append(line('$'), ['Goal: Finish it without any mistakes to see what it outputs!', ''])
  endif

  call append(line('$'), ['Characters Per Minute:   ' . string(g:ttt_cpm)])
  call append(line('$'), ['Words Per Minute:        ' . string(g:ttt_wpm), ''])

  call append(line('$'), ['Press one of these to continue:'])
  call append(line('$'), '  -     Choose a different activity or topic')
  call append(line('$'), '  t     Try this activity again')
  call append(line('$'), ['  ZZ    Quit', ''])

  call append(line('$'), split(output, "\n"))

  set nomodified

  call Colorizer#ColorToggle()

  exe g:ttt_cur.window . 'wincmd w'
  exe g:ttt_cur.buffer . 'buffer'
  " call winrestview(g:ttt_cur.view)

  if g:ttt_success
    call touchtutor#RunFile()
  endif
endfunction

function! touchtutor#RunFile()
  " @TODO UPDATE TO SHOW OUTPUT IN ANOTHER PANE
  let fileDir = expand('%:p:h')
  let runscript = fileDir . '/.run.vim'

  if filereadable(expand(runscript))
    exe 'source ' . runscript
  endif

  " Create output buffer if it doesn't exist
  if tttpanels#IsOpen('stdout') == 0
    let g:ttt_outs.stdout = tttpanels#CreateOutput('right', get(g:ttt_run, 'synt', 'none'))
  endif

  silent exe 'file BONUS\ OUTPUT:\ '. escape(g:ttt_cur.tail, ' |\"')

  silent normal! gg0"_dG

  call setline(1, [
        \ 'Congratulations!',
        \ '',
        \ 'Here is what your program outputted:',
        \ '',
        \ '----------',
        \ ''])

  call append(line('$'), get(g:ttt_run, 'stdout', ['No Output']))

  set nomodified

  call Colorizer#ColorToggle()

  exe g:ttt_cur.window . 'wincmd w'
  exe g:ttt_cur.buffer . 'buffer'
  " call winrestview(g:ttt_cur.view)
endfunction

function! touchtutor#SaveAndTest()
  call ttttimer#StopCounting()
  silent iunmap <CR>
  silent iunmap <ESC>
  write
  call touchtutor#Finished()
endfunction

function! touchtutor#SkipLine()
  if line('.') == line('$') && col('.') == col('$')
    stopinsert
    call touchtutor#SaveAndTest()
  endif
endfunction

function! touchtutor#StartTesting()
  call tttpanels#CloseOut('stdout')
  call tttpanels#CloseOut('result')

  inoremap <ESC> <ESC>:call touchtutor#SaveAndTest()<CR>
  inoremap <CR> <C-\><C-o>:call touchtutor#SkipLine()<CR><CR>

  " Begin the test!
  silent normal! gg0
  edit!
  set bufhidden=delete
  startgreplace
  call ttttimer#StartCounting()
endfunction

" autocmd BufWritePost * call touchtutor#Finished()
autocmd BufReadPost * call touchtutor#StartTesting()
" autocmd InsertLeave * call g:ttt_saveFile()

" @link https://vim.fandom.com/wiki/Disable_automatic_comment_insertion#Disabling_in_general
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

nnoremap - :Explore<CR>
nnoremap ZZ :qa!<CR>
nnoremap t :call touchtutor#StartTesting()<CR>

