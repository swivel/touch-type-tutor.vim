#!/usr/bin/env bash

if [ $# -ne 1 ]; then
	echo "No arguments passed..."
	exit
fi

git reset $1 > /dev/null 2>&1
git add $1 > /dev/null 2>&1

# ref: http://git.661346.n2.nabble.com/PATCH-1-2-t4034-diff-words-replace-regex-for-diff-driver-td7177053.html
output=$(git diff -R --cached -b --ignore-cr-at-eol --color-words="[^[:space:]]|([[:alnum:]]|UTF_8_GUARD)+" $1 2>&1)
#output=$(git diff -R --cached $1 2>&1)

git reset $1 > /dev/null 2>&1
git checkout -- $1 > /dev/null 2>&1

echo "$output"

