# VIM Touch Type Tutor

>
> Dedicated to my children,
> 
> - Gabriel Alexander Dalrymple
> - Savannah Marie Dalrymple
> 
> May you never cease to embrace radical creativity, and continue to grow in wisdom and understanding.
> 
> I couldn't ask for better children.
> 
> Love,
> _Your Father_
>


This project aims at creating a very simple and minimalist Touch Type Tutor environment using VIM.  This allows anyone to hop in and learn how to Touch Type in VIM to improve their skills, not only with the keyboard in various scenarios, but also with the basics of navigating VIM itself.

## Get Started

Make sure you have the following prerequisites installed:

**Prerequisites**

 1. \*nix
 2. vim
 3. git


### 1. Clone this repo!

In a terminal, run the following:
```
git clone https://gitlab.com/swivel/touch-type-tutor.vim.git
cd touch-type-tutor.vim
```


### 2. Run the tutor

```
./run.sh
```


## What now?

Before we get started, at any point in time you can quit by pressing <kbd>Esc</kbd> (Escape key) and then press <kbd>Shift</kbd>-<kbd>Z</kbd>-<kbd>Z</kbd> (or, in vim-speak: "in normal mode, press `ZZ`")

### Choose a topic

The first thing you should see is a list of potential topics. What you're actually seeing is a mini-explorer built-in to `vim` called `netrw`. That may sound scary, but it's actually really simple to navigate.

 - Press <kbd>Enter</kbd> to choose a category


### Choose an activity

Once you've chosen a category, you can now choose an activity. You're still in `netrw`, so the same controls work:

 - Press <kbd>Enter</kbd> to choose a activity
 - Press <kbd>-</kbd> _(minus)_ to go back


### Start typing!

Once you've chosen an activity, it'll throw you right in!  Start typing the letters and symbols you see on the screen and see how fast you can type them without making mistakes.

Once you get to the end, it will prompt you with your results.

At any time, you can stop the test by pressing <kbd>Esc</kbd> and go to the section below this paragraph. Pressing the escape will put `vim` into "Normal" mode, which stops the typing.


### Choose another one or try again

> _Are you in "Normal" mode? If you're not sure, hit <kbd>Esc</kbd>._

If you're ready to move on to the next activity, you can get back to `netrw` by pressing the <kbd>-</kbd> key again.

If you'd like to try again, Touch Type Tutor itself lets you press <kbd>t</kbd> in order to Try Again.


### All Done? Quit!

At any point in time you can quit by pressing <kbd>Esc</kbd> (Escape key) and then press <kbd>Shift</kbd>-<kbd>Z</kbd>-<kbd>Z</kbd> (or, in vim-speak: "in normal mode, press `ZZ`")


